# Codementor: A Dummy’s Guide to Redux and Thunk in React
This is an implementation of a tutorial by Matt Stow, the tutorial is freely avaliable at: https://medium.com/@stowball/a-dummys-guide-to-redux-and-thunk-in-react-d8904a7005d3, the article describes the basics of using Redux and Thunt in a React app, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)
- npm available when installing [Node.js](https://nodejs.org/en/)

## Installing

just clone this repository and run npm install by using these commands:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Redux_Thunk_Medium.git
cd Redux_Thunk_Medium/src
npm install
```

## Running
This example uses react scripts and can be ran by typing this command
```
cd src/
npm start
```

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source and tutorial by [Matt Stow](https://github.com/stowball) available at: https://medium.com/@stowball/a-dummys-guide-to-redux-and-thunk-in-react-d8904a7005d3
